// TODO: balance
// TODO: buff icon for blessed effect
// TODO: effect when the "beam" is broken

module.exports.info = {
	name: 'Blessed Light',
	description: 'Reflect holy light off a nearby friend, renewing Blessed effect until the beam is broken.',
	type: 'blessedLight',
	spritesheet: 'server/mods/mo-runes-mod/images/spellIcons.png',
	icon: [1, 1],
	animation: 'hitStaff',
	spellType: 'buff',
	row: 11,
	col: 4,
	// TODO: Should speed be constant or dependent on distance?
	speed: 1000,
	// TODO: update particles to differentiate from standard projectile attack
	particles: {
		color: {
			start: ['ffeb38', 'ff6942'],
			end: ['ff6942', 'd43346']
		},
		scale: {
			start: {
				min: 2,
				max: 14
			},
			end: {
				min: 0,
				max: 8
			}
		},
		lifetime: {
			min: 1,
			max: 1
		},
		alpha: {
			start: 0.5,
			end: 0
		},
		randomScale: true,
		randomColor: true,
		chance: 0.25
	}
};

module.exports.config = {
	// Magic Missile Config: (for reference)
	// statType: 'int',
	// statMult: 1,
	// element: 'arcane',
	// cdMax: 6,
	// castTimeMax: 8,
	// manaCost: 4,
	// range: 9,
	// random: {
	// 	damage: [4, 15]
	// }

	statType: ['int'],
	element: 'holy',
	cdMax: 6,
	castTimeMax: 6,
	manaCost: 4,
	range: 5,
	random: {
		i_regenHp: [5, 15],
		i_duration: [15, 40]
	}
};

module.exports.template = {
	type: 'blessedLight',

	// TODO: check if necessary
	cooldown: 10,

	needLos: true,

	targetFriendly: true,
	//targetGround: false,

	// Copied from normal projectile
	row: 3,
	col: 0,

	cast: function (action) {
		// First target is the user's choice
		let target = action.target;

		// Can't cast again if you already have the effect to avoid multiple projectiles bouncing
		// TODO: somehow detect too many particles bouncing around and merge/cull them?
		if (this.obj.effects.effects.find(e => e.type === 'blessed') || this.obj.effects.effects.find(e => e.type === 'blessed')) {
			return true;
		}

		// Target must be a player
		if (target && target.auth) {
			// Bump in the direction of the target
			this.sendBump(target);

			// Activate the effect on the caster and pass on
			// this.onReachTarget(this.obj, target, 4, true);

			// Fire from the original caster
			this.fireProjectile(this.obj, target, 1000);

			return true;
		}

		// effectCasting/spellbook locks up and prevents movement if we don't return a "success"
		// We "succeed" even if we failed to cast
		return true;
	},

	// Called when the projectile hits the target ("explode")
	onReachTarget: function (origin, newCaster, num) {
		// Apply regen to the hit player (excluding the caster)
		let effect = newCaster.effects.effects.find(e => e.type === 'blessed');
		if (effect) {
			effect.ttl = this.values.duration;
		} else {
			effect = newCaster.effects.addEffect({
				type: 'blessed',
				ttl: this.values.duration,
				amount: this.values.regenHp,
				noMsg: false
			});
		}

		// Continue bouncing
		if (num > 0) {
			let newTarget = this.getTarget(newCaster);
			// Re-check LoS
			if (newTarget && this.obj.instance.physics.hasLos(newCaster.x, newCaster.y, newTarget.x, newTarget.y)) {
				this.fireProjectile(newCaster, newTarget, num);
			}
		}
	},

	fireProjectile: function (caster, target, num) {
		// let ttl = (Math.sqrt(Math.pow(target.x - caster.x, 2) + Math.pow(target.y - caster.y, 2)) * this.speed) - 50;
		let ttl = this.speed;

		let projectileConfig = {
			caster: caster.id,
			components: [{
				idSource: caster.id,
				idTarget: target.id,
				type: 'projectile',
				ttl: ttl,
				projectileOffset: this.projectileOffset,
				particles: this.particles
			}, {
				type: 'attackAnimation',
				layer: 'projectiles',
				loop: -1,
				row: this.row,
				col: this.col
			}]
		};

		this.obj.fireEvent('beforeSpawnProjectile', this, projectileConfig);

		this.sendAnimation(projectileConfig);

		this.queueCallback(this.onReachTarget.bind(this, caster, target, num - 1), ttl, null, target);
	},

	getTarget: function (obj) {
		// Find all other players (have auth component) that aren't the current caster
		let inRange = obj.instance.physics.getArea(
			obj.x - this.range,
			obj.y - this.range,
			obj.x + this.range,
			obj.y + this.range,
			c => c.auth && c.id !== obj.id
		);

		// Find the closest inRange player
		let index = 0;
		let best = 1000;
		for (let i = 0; i < inRange.length; i++) {
			let f = inRange[i];

			let distance = Math.sqrt(Math.pow(f.x - obj.x, 2) + Math.pow(f.y - obj.y, 2));

			if (distance < best) {
				best = distance;
				index = i;
			}
		}

		// Return it if they exist
		if (inRange.length > 0) {
			return inRange[index];
		}
	}
};

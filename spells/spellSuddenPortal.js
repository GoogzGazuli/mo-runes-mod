// TODO: balance
// TODO: minimum distance to teleport?

module.exports.info = {
	name: 'Sudden Portal',
	description: 'Teleport to target destination, dealing damage at the start and end portals.',
	type: 'suddenPortal',
	spritesheet: 'server/mods/mo-runes-mod/images/spellIcons.png',
	icon: [0, 2],
	animation: 'raiseStaff',
	speed: 150
};

module.exports.config = {
	statType: ['int'],
	element: 'arcane',
	cdMax: 12,
	castTimeMax: 1,
	range: 10,
	manaCost: 4,
	random: {
		damage: [2, 5],
		i_radius: [1, 2.2],
		i_pushback: [2, 5]
	}
};

module.exports.template = {
	type: 'suddenPortal',

	cdMax: 20,
	manaCost: 10,
	range: 9,

	damage: 5,

	radius: 2,
	pushback: 4,

	targetGround: true,
	needLos: true,

	particleColorA: {
		start: ['3a71ba', '42548d'],
		end: ['48edff', '3fa7dd']
	},
	particleColorB: {
		start: ['533399', '393268'],
		end: ['a24eff', '7a3ad3']
	},
	portalParticles: {
		scale: {
			start: {
				min: 4,
				max: 24
			},
			end: {
				min: 0,
				max: 12
			}
		},
		frequency: 0.02,
		emitterLifetime: 0.15,
		spawnType: 'circle',
		lifetime: {
			min: 1,
			max: 2
		},
		spawnCircle: {
			x: 0,
			y: 0,
			r: 8
		},
		speed: {
			start: {
				min: 4,
				max: 24
			},
			end: {
				min: 0,
				max: 12
			}
		},
		randomSpeed: true,
		randomScale: true,
		randomColor: true
	},
	teleportParticles: {
		color: {
			start: ['3a71ba', '42548d'],
			end: ['48edff', '3fa7dd']
		},
		scale: {
			start: {
				min: 2,
				max: 14
			},
			end: {
				min: 0,
				max: 8
			}
		},
		lifetime: {
			min: 1,
			max: 1
		},
		alpha: {
			start: 0.5,
			end: 0
		},
		randomScale: true,
		randomColor: true,
		chance: 0.25
	},

	cast: function (action) {
		// Calculate TTL
		let x = this.obj.x;
		let y = this.obj.y;
		let target = {
			x: action.target.x,
			y: action.target.y
		};
		let distance = Math.sqrt(Math.pow(target.x - x, 2) + Math.pow(target.y - y, 2));
		let ttl = distance * this.speed;

		// Can't move, attack, or cast
		let selfEffect = this.obj.effects.addEffect({
			type: 'stunned',
			noMsg: true
		});

		// Bump in the direction of the target
		this.sendBump(target);

		// Portal blast
		this.portalBlast(x, y, true);

		// Hide the player for a "teleport" appearance
		this.obj.hidden = true;
		this.obj.syncer.o.hidden = true;

		// Remove from physics ("teleporting")
		this.obj.instance.physics.removeObject(this.obj, this.obj.x, this.obj.y);

		// "Teleport" projectile
		let projectileConfig = {
			caster: this.obj.id,
			components: [{
				idSource: this.obj.id,
				target: target,
				type: 'projectile',
				// Tweak timing
				ttl: ttl - 50,
				particles: this.teleportParticles
			}, {
				type: 'attackAnimation',
				layer: 'projectiles',
				noSprite: true,
				loop: -1,
				spritesheet: 'server/mods/mo-runes-mod/images/projectiles.png',
				row: 0,
				col: 0
			}]
		};
		this.obj.fireEvent('beforeSpawnProjectile', this, projectileConfig);
		this.sendAnimation(projectileConfig);

		// Callback for when the projectile arrives + some extra delay
		this.queueCallback(this.reachDestination.bind(this, target, selfEffect), ttl + 50);

		// Successful cast, otherwise we get stuck in casting state
		return true;
	},
	reachDestination: function (target, selfEffect) {
		// If we die while "teleporting"
		if (this.obj.destroyed) {
			return;
		}

		// Unhide
		this.obj.hidden = false;
		this.obj.syncer.o.hidden = false;

		// Update position
		this.obj.x = target.x;
		this.obj.y = target.y;

		// Syncer update position
		let syncer = this.obj.syncer;
		syncer.o.x = target.x;
		syncer.o.y = target.y;

		// Add back to physics
		this.obj.instance.physics.addObject(this.obj, this.obj.x, this.obj.y);

		// Remove stun on self (can move, cast, attack again)
		this.obj.effects.removeEffect(selfEffect, true);

		// Update aggro (?)
		this.obj.aggro.move();

		// Portal blast
		this.portalBlast(target.x, target.y, false);
	},
	// Tweaked from Fireblast
	portalBlast: function (x, y, isA) {
		// Colors from Isleward palette
		let color = this.particleColorA;
		if (!isA) {
			color = this.particleColorB;
		}

		let particles = extend({}, this.portalParticles);
		particles.color = color;

		let radius = this.radius;

		let physics = this.obj.instance.physics;
		let syncer = this.obj.instance.syncer;

		for (let i = x - radius; i <= x + radius; i++) {
			for (let j = y - radius; j <= y + radius; j++) {
				if (!physics.hasLos(~~x, ~~y, ~~i, ~~j)) {
					continue;
				}

				let effect = {
					x: i,
					y: j,
					components: [{
						type: 'particles',
						ttl: 10,
						blueprint: particles
					}]
				};

				if ((i !== x) || (j !== y)) {
					syncer.queue('onGetObject', effect, -1);
				}

				let mobs = physics.getCell(i, j);
				let mLen = mobs.length;
				for (let k = 0; k < mLen; k++) {
					let m = mobs[k];

					// Maybe we killed something?
					if (!m) {
						mLen--;
						continue;
					}

					if ((!m.aggro) || (!m.effects)) {
						continue;
					}

					if (!this.obj.aggro.canAttack(m)) {
						continue;
					}

					let targetEffect = m.effects.addEffect({
						type: 'stunned',
						noMsg: true
					});

					let targetPos = {
						x: m.x,
						y: m.y
					};

					// Find out where the mob should end up
					let dx = m.x - this.obj.x;
					let dy = m.y - this.obj.y;

					while ((dx === 0) && (dy === 0)) {
						dx = ~~(Math.random() * 2) - 1;
						dy = ~~(Math.random() * 2) - 1;
					}

					dx = ~~(dx / Math.abs(dx));
					dy = ~~(dy / Math.abs(dy));
					for (let l = 0; l < this.pushback; l++) {
						if (physics.isTileBlocking(targetPos.x + dx, targetPos.y + dy)) {
							if (physics.isTileBlocking(targetPos.x + dx, targetPos.y)) {
								if (physics.isTileBlocking(targetPos.x, targetPos.y + dy)) {
									break;
								} else {
									dx = 0;
									targetPos.y += dy;
								}
							} else {
								dy = 0;
								targetPos.x += dx;
							}
						} else {
							targetPos.x += dx;
							targetPos.y += dy;
						}
					}

					let distance = Math.max(Math.abs(m.x - targetPos.x), Math.abs(m.y - targetPos.y));
					let ttl = distance * 125;

					m.clearQueue();

					this.sendAnimation({
						id: m.id,
						components: [{
							type: 'moveAnimation',
							targetX: targetPos.x,
							targetY: targetPos.y,
							ttl: ttl
						}]
					});

					let damage = this.getDamage(m);
					m.stats.takeDamage(damage, 1, this.obj);

					if (!m.destroyed) {
						physics.removeObject(m, m.x, m.y);
						this.queueCallback(this.portalBlastEnd.bind(this, m, targetPos, targetEffect), ttl, null, m);
					}
				}
			}
		}
	},
	portalBlastEnd: function (target, targetPos, targetEffect) {
		target.effects.removeEffect(targetEffect, true);

		target.x = targetPos.x;
		target.y = targetPos.y;

		let syncer = target.syncer;
		syncer.o.x = targetPos.x;
		syncer.o.y = targetPos.y;

		target.instance.physics.addObject(target, target.x, target.y);
	}
};

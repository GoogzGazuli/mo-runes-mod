
module.exports = {
	name: 'Mo Runes',

	spells: [],

	/*
	extraScripts: [
		'spells/spellBlessedLight'
	],
	*/

	init: function () {
		[
			"BlessedLight",
			"SuddenPortal"
		].forEach(s => {
			this.spells.push(require('./spells/spell'+s))
		});

		this.events.on('onBeforeGetSpellsInfo', this.beforeGetSpellsInfo.bind(this));
		this.events.on('onBeforeGetSpellsConfig', this.beforeGetSpellsConfig.bind(this));
		this.events.on('onBeforeGetSpellTemplate', this.beforeGetSpellTemplate.bind(this));

		this.events.on('onBeforeGetEffect', this.onBeforeGetEffect.bind(this));

		this.events.on('onBeforeGetResourceList', this.beforeGetResourceList.bind(this));
	},

	beforeGetResourceList: function (resources) {
		resources.push('server/mods/mo-runes-mod/images/projectiles.png')
	},

	beforeGetSpellsInfo: function (spells) {
		this.spells.forEach(s => {
			spells.push(s.info);
		})
	},

	beforeGetSpellTemplate: function (spell) {
		this.spells.forEach(s => {
			if (spell.type.toLowerCase() === s.template.type.toLowerCase()) {
				spell.template = s.template;
			}
		})
	},

	beforeGetSpellsConfig: function (spells) {
		this.spells.forEach(s => {
			spells[s.info.name.toLowerCase()] = s.config;
		})
	},

	onBeforeGetEffect: function (result) {
		if (result.type.toLowerCase() === 'blessed') {
			result.url = `${this.relativeFolderName}/effects/effectBlessed.js`;
		}
	}
};

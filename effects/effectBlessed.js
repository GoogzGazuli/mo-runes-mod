// differentiate other sources of regenHp

module.exports = {
	type: 'blessed',

	amount: 1,

	init: function () {
		this.obj.stats.addStat('regenHp', this.amount);
	},

	destroy: function () {
		this.obj.stats.addStat('regenHp', -1 * this.amount);
	},

	update: function () {

	},

	events: {

	}
};
